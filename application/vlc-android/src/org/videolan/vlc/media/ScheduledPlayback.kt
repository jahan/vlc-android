package org.videolan.vlc.media

import android.net.Uri
import androidx.core.net.toFile
import org.videolan.libvlc.interfaces.IMedia
import org.videolan.vlc.util.getParentFolder
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

/*Ported from C code to Kotlin, needs refactoring to be Kotlin friendly*/

val EXTENSIONS = listOf(".asx", ".b4s", ".cue", ".ifo", ".m3u", ".m3u8", ".pls", ".ram", ".rar",
        ".sdp", ".vlc", ".xspf", ".wax", ".wvx", ".zip", ".conf")
const val WEEKDAYS = "MTWTFSS";
val WEEKDAYS_LONG = listOf("MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY")

const val INCLUDES = "includes"
const val EXCLUDES = "excludes"

fun scheduledPlayback(media: IMedia?): Short {
    var result: Short = 1
    if (media?.uri?.toString()?.contains("file")!!) {
        val files = findApplicableRuleFiles(media.uri)
        result = checkRules(media.uri, files)
    }
    return result
}

private fun checkRules(mediaUri: Uri, files: Set<String>): Short {
    val playRules = mutableListOf<PlayRule>()
    var result: Short = 0

    for (el in files) {
        val ext = el.split(".")[1]
        if (EXTENSIONS.contains(ext) || mediaUri.toFile().isDirectory) {
            //skippd extension
            result = 1
            return result
        }
        val lines = File(el).readLines()
        var i = 0
        while (i < lines.size) {
            val start = lines[i].substring(0..4)
            val end = lines[i].substring(6..10)
            val weekdays = if (lines[i].substring(12..18).isNotBlank()) lines[i].substring(12..18) else "MTWTFSS"
            val includes = mutableListOf<String>()
            val excludes = mutableListOf<String>()
            i++
            i = readIncludePatterns(lines, i, includes)
            i = readExcludePatterns(lines, i, excludes)
            val playRule = PlayRule(start, end, weekdays, el, includes, excludes)
            playRules.add(playRule)
        }
    }

    for (playRule in playRules) {
        val calendar = Calendar.getInstance()
        val dayName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault())
        val weekday = WEEKDAYS_LONG.indexOf(dayName?.toUpperCase())
        if (playRule.weekdays[weekday] == WEEKDAYS[weekday]) {
            continue
        } else {
            val time = SimpleDateFormat("hh:mm", Locale.US).format(calendar.time)
            if ((time > playRule.start).and(time < playRule.end)) {
                var j = 0
                while (j < playRule.includes.size) {
                    if (playRule.includes[j].startsWith("*")) {
                        val group = Regex("[*/]*([\\w,\\s-.]+)\\**").matchEntire(playRule.includes[j])?.groups?.get(1)?.value
                        val match = if (group != null) mediaUri.toString().contains(group, true) else false
                        if (playRule.includes[j] == "**/**")
                            result = 0
                        else if (match) {
                            result = 1
                            return result
                        }
                    }
                    j++
                }
                j = 0
                while (j < playRule.excludes.size) {
                    if (playRule.excludes[j].startsWith("*")) {
                        val group = Regex("[*/]*([\\w,\\s-.]+)\\**").matchEntire(playRule.excludes[j])?.groups?.get(1)?.value
                        val match = if (group != null) mediaUri.toString().contains(group, true) else false
                        if (playRule.excludes[j] == "**/**")
                            result = 0
                        else if (match) {
                            result = -1
                            return result
                        }
                    }
                    j++
                }
            }
        }
    }
    return result
}

private fun readExcludePatterns(lines: List<String>, i: Int, excludes: MutableList<String>): Int {
    var idx = i
    if (lines[idx].startsWith(EXCLUDES)) {
        idx++
        while (idx < lines.size) {
            if (Regex("\\d{2}:\\d{2}-\\d{2}:\\d{2}").containsMatchIn(lines[idx]))
                break
            excludes.add(lines[idx])
            idx++
        }
    }
    return idx
}

private fun readIncludePatterns(lines: List<String>, i: Int, includes: MutableList<String>): Int {
    var idx = i
    if (lines[idx].startsWith(INCLUDES)) {
        idx++
        while (!lines[idx].startsWith(EXCLUDES) && idx < lines.size &&
                !Regex("\\d{2}:\\d{2}-\\d{2}:\\d{2}").containsMatchIn(lines[idx])) {
            includes.add(lines[idx])
            idx++
        }
    }
    return idx
}

class PlayRule(val start: String, val end: String, val weekdays: String, val folderPath: String, val includes: List<String>, val excludes: List<String>)

private fun findApplicableRuleFiles(uri: Uri): Set<String> {
    var parentFolder = uri.toFile().absoluteFile.absolutePath
    val ruleFilePath = parentFolder.plus("/rule.mrl")
    val ruleFiles = mutableSetOf<String>()
    if (File(ruleFilePath).exists())
        ruleFiles.add(ruleFilePath)
    while (File(parentFolder.getParentFolder()).exists() && parentFolder != "/") {
        val ruleFilePath = parentFolder.plus("/rule.mrl")
        if (File(ruleFilePath).exists())
            ruleFiles.add(ruleFilePath)
        parentFolder = parentFolder.getParentFolder()
    }
    return ruleFiles
}
